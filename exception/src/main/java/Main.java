/**
 * Created by Администратор on 10.09.2019.
 */
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.Optional;

public class Main {

    public static void main(String[] args) throws Exception {
        try (AutoClose resource = new AutoClose()) {
            new LifeCycleAction().execute();


        } catch (LifeCycleActionExecutionException | AccessDeniedException | MyException e) {


            System.err.println(e.getLocalizedMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        try (AutoClose autoClose = new AutoClose()) {
            new Runner();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void returnValueForException(String s) {
    }

    public static class LifeCycleAction {
        public void execute() throws LifeCycleActionExecutionException, AccessDeniedException, MyException {
            throw new MyException("This is Exception!");

        }
    }

    public static class LifeCycleActionExecutionException extends Exception {
    }

    public static class MyException extends Exception {
        public MyException(String s) {

        }
    }

    public void exceptionVsResult() {
        final String result1 = (String) this.returnResult().value;
        final String result2 = returnOptional().orElse("");
        String result3 = "";
        try {
            result3 = returnValueOrThrowException();
        } catch (AccessDeniedException e) {
        }
    }

    private Result returnResult() {
        return Result.OK.setValue("OK");
    }

    private Optional<String> returnOptional() {
        return Optional.of("OK");
    }

    private String returnValueOrThrowException() throws AccessDeniedException {
        return "OK";
    }


}


