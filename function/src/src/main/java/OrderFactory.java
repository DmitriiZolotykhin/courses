﻿/**
 * Created by Администратор on 08.09.2019.
 */

public interface OrderFactory {
    default Order addNotStartedOrder() {
        return new Order(OrderStatus.NOT_STARTED);
    }

    default Order addProcessingOrder() {
        return new Order(OrderStatus.PROCESSING);
    }

    default Order addCompletedOrder() {
        return new Order(OrderStatus.COMPLETED);
    }

    default Order addRandomStatusOrder() {
        return new Order(OrderStatus.getRandomOrderStatus());
    }

    default Order addGiveDiscount() {
        return new Order(GiveDiscount.NOT_DISCOUNT);
    }


}

