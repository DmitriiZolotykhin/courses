import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.System.*;

public class Main {
    public static void main(String[] args) {
        Collection<String> c = Collections.EMPTY_LIST;
        List<String> list = new ArrayList<>(c);
        List<Order> orders = Collections.singletonList(new Order(OrderStatus.COMPLETED));
        orders.stream()
                .filter(order -> order.status == OrderStatus.COMPLETED)

                .filter(discount -> (order.priceRandom() > 50) && (discount.discount == GiveDiscount.DISCOUNT));


        for (Order order : orders) {
            out.println(order.toString());
        }
        for (Iterator<Order> iterator = orders.iterator(); iterator.hasNext(); ) {
            out.println(iterator.next().toString());
        }
        Map<OrderStatus, List<Order>> ordersByStatus = orders.stream()
                .collect(Collectors.groupingBy(Order::getStatus));

    }

    public enum GiveDiscount

    {
        DISCOUNT, NOT_DISCOUNT
    }

    public enum OrderStatus

    {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    public static class Order {
        public final OrderStatus status;
        public double price;
        public GiveDiscount discount;

        public Order(OrderStatus status) {
            this.status = status;
        }

        public OrderStatus getStatus() {
            return status;
        }

        public double priceRandom() {
            return (price = 100 * Math.random());
        }

    }
}

