import java.io.*;

public class Main2 {
    public static void main(String[] args) {
        String path = "text.txt";
        String path2 = "text2.txt";
        File file = new File(path);
        File fileResult = new File(path2);
        String result = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file)); BufferedWriter bw = new BufferedWriter(new FileWriter(fileResult))) {
            String str = "";
            while ((str = br.readLine()) != null) {
                bw.write(new StringBuilder(str).reverse().append(System.lineSeparator()).toString());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}