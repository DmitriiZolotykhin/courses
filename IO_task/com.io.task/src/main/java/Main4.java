import java.util.stream.Stream;

public class Main4 {


        public static void main (String[] agr){
            Stream.of(new Student("Max"),new Student("Mike")).map(Student::getName).forEach(System.out::println);
        }
    }
    class Student {
        String name;

        public Student(String name){
            this.name=name;
        }

        public String getName(){
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


