import java.io.*;
import java.nio.charset.StandardCharsets;

public class Main3 {


    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("textDonut.txt", true);
        String name = "Пончик";
        Float price = 20000F;
        int count = 22;
        String greetings = " " + name + " " + price + " " + count;
        fileOutputStream.write(greetings.getBytes());
        fileOutputStream.close();
        FileInputStream fileInputStream = new FileInputStream("textDonut.txt");
        int i;
        byte[] line = new byte[fileInputStream.available()];
        fileInputStream.read(line);
        System.out.println(new String(line, StandardCharsets.UTF_8));
    }
}





