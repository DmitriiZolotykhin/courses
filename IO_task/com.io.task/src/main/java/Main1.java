import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main1 {
    public static void main(String[] args) throws IOException {
        String str = (new String(Files.readAllBytes(Paths.get("text.txt"))));
        String reverse = new StringBuffer(str).reverse().toString();
        System.out.println("\nТекст из файла до реверса: " + str);
        System.err.println("Текст из файла в обратном порядке, после реверса: " + reverse);
        System.out.println();

    }


}
