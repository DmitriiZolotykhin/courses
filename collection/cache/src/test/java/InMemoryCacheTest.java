/**
 * Created by Администратор on 07.09.2019.
 */

public class InMemoryCacheTest {

    public static void main(String[] args) throws InterruptedException {

        InMemoryCacheTest Cache = new InMemoryCacheTest();

        System.out.println("-- Test1: Добавление и удаление объектов --");
        Cache.TestAddRemoveObjects();
        System.out.println("-- Test2: Тест для объектов с истекшим кэшем -- ");
        Cache.TestExpiredCacheObjects();
        System.out.println("-- Test3: Время очистки объектов -- ");
        Cache.TestObjectsCleanupTime();
    }

    private void TestAddRemoveObjects() {

        InMemoryCache<String, String> cache = new InMemoryCache<String, String>(200, 500, 6);
        cache.put("Samsung", "Samsung");
        cache.put("Sony", "Sony");
        cache.put("Philips", "Philips");
        cache.put("Apple", "Aplle");
        cache.put("Sven", "Sven");
        cache.put("MicroLab", "Microlab");

        System.out.println("6 объектов добавлено в кэш.. cache.size(): " + cache.size());
        cache.remove("Apple");
        System.out.println("Один объект удален.. cache.size(): " + cache.size());

        cache.put("Twitter", "Twitter");
        cache.put("SAP", "SAP");
        System.out.println("Добавлены два объекта, но достигнут максимум.. cache.size(): " + cache.size());
        System.out.println();

    }

    private void TestExpiredCacheObjects() throws InterruptedException {
        InMemoryCache<String, String> cache = new InMemoryCache<String, String>(1, 1, 10);
        cache.put("eBay", "eBay");
        cache.put("Paypal", "Paypal");
        Thread.sleep(3000); // если добавим TimeToLive=10000 добавяться два объект

        System.out.println("Два объекта добавлено, но время жизни кэша истекло. cache.size(): " + cache.size());

    }

    private void TestObjectsCleanupTime() throws InterruptedException {
        int size = 500000;

        InMemoryCache<String, String> cache = new InMemoryCache<String, String>(100, 100, 500000);

        for (int i = 0; i < size; i++) {
            String value = Integer.toString(i);
            cache.put(value, value);
        }

        Thread.sleep(200);

        long start = System.currentTimeMillis();
        cache.cleanup();
        double finish = (double) (System.currentTimeMillis() - start) / 1000.0;

        System.out.println(" " + size + " objects are " + finish + " s");
        System.out.println("Время очистки кеша для " + size + " объектов " + finish + " s");

    }
}
