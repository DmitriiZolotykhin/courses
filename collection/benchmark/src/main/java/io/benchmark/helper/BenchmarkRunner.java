package io.benchmark.helper;

import java.util.*;

public class BenchmarkRunner {
    private ListBenchmarkRunner listBenchmarkRunner;
    private QueueBenchmarkRunner queueBenchmarkRunner;
    private int iterations;

   public void setListBenchmarkRunner(ListBenchmarkRunner listBenchmarkRunner) {
       this.listBenchmarkRunner = listBenchmarkRunner;
    }


    public void setQueueBenchmarkRunner(QueueBenchmarkRunner queueBenchmarkRunner) {
        this.queueBenchmarkRunner = queueBenchmarkRunner;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public Map runTests(){
        Map<String, Map<String, Long>> testOperationsResults = new LinkedHashMap<String, Map<String, Long>>();
        Vector<Integer> vector = new Vector<Integer>();
        Queue<Integer> queue = new PriorityQueue<Integer>();
        testOperationsResults.put("Vector", listBenchmarkRunner.performTests(vector, iterations));
        testOperationsResults.put("Queue", queueBenchmarkRunner.performTests(queue, iterations));
        return testOperationsResults;
    }
}
