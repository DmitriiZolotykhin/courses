package io.benchmark.tests.list;

import io.benchmark.tests.Position;

import java.util.List;


public interface ListTest {
    void executeAddElements(List<Integer> list);
    void executeInsertElements(List<Integer> list, Position pos, int elemCount);
    void executeGetElementsConsequently(List<Integer> list, Position pos, int elemCount);
    void executeGetElementsRandomly(List<Integer> list, int elemCount);
    void executeRemoveElements(List<Integer> list, Position pos, int elemCount);
}
