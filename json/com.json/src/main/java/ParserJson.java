import java.io.*;
import javax.json.Json;
import javax.json.JsonValue;
import javax.json.stream.JsonParser;


public class ParserJson {

    public static void main(String[] args) {
            File file = new File("example.json");
            String nodeName = null;
            String nodeType = null;
            String nodeValue = null;
            try(InputStream is = new FileInputStream(file);
                JsonParser parser = Json.createParser(is)) {
                while (parser.hasNext()) {
                    JsonParser.Event event = parser.next();
                    if (event == JsonParser.Event.KEY_NAME) {
                        if ((nodeName = parser.getString()).equals("Brand")) {
                            event = parser.next();
                            JsonValue value = parser.getValue();
                            nodeType = value.getValueType().toString();
                            nodeValue = value.toString();
                        }
                    }
                }
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.out.printf("Node with name %s has type %s and value: %s", nodeName, nodeType, nodeValue);
        }
    }











