package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Brand implements Serializable {
    @SerializedName("BrandName")
        private String BrandName;
    @SerializedName("Branch")
        private Branch[] Branch;

        public String getBrandName ()
        {
            return BrandName;
        }

        public void setBrandName (String BrandName)
        {
            this.BrandName = BrandName;
        }

        public Branch[] getBranch ()
        {
            return Branch;
        }

        public void setBranch (Branch[] Branch)
        {
            this.Branch = Branch;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [BrandName = "+BrandName+", Branch = "+Branch+"]";
        }
    }


