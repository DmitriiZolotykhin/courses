package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostalAddress implements Serializable {
    @SerializedName("CountrySubDivision")
        private String[] CountrySubDivision;
    @SerializedName("AddressLine")
        private String[] AddressLine;
    @SerializedName("TownName")
        private String TownName;
    @SerializedName("Country")
        private String Country;
    @SerializedName("GeoLocation")
        private GeoLocation GeoLocation;
    @SerializedName("PostCode")
        private String PostCode;

        public String[] getCountrySubDivision ()
        {
            return CountrySubDivision;
        }

        public void setCountrySubDivision (String[] CountrySubDivision)
        {
            this.CountrySubDivision = CountrySubDivision;
        }

        public String[] getAddressLine ()
        {
            return AddressLine;
        }

        public void setAddressLine (String[] AddressLine)
        {
            this.AddressLine = AddressLine;
        }

        public String getTownName ()
        {
            return TownName;
        }

        public void setTownName (String TownName)
        {
            this.TownName = TownName;
        }

        public String getCountry ()
        {
            return Country;
        }

        public void setCountry (String Country)
        {
            this.Country = Country;
        }

        public GeoLocation getGeoLocation ()
        {
            return GeoLocation;
        }

        public void setGeoLocation (GeoLocation GeoLocation)
        {
            this.GeoLocation = GeoLocation;
        }

        public String getPostCode ()
        {
            return PostCode;
        }

        public void setPostCode (String PostCode)
        {
            this.PostCode = PostCode;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [CountrySubDivision = "+CountrySubDivision+", AddressLine = "+AddressLine+", TownName = "+TownName+", Country = "+Country+", GeoLocation = "+GeoLocation+", PostCode = "+PostCode+"]";
        }
    }


