package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Branch implements Serializable {


    @SerializedName("Availability")
        private Availability Availability;

    @SerializedName("PostalAddress")
        private PostalAddress PostalAddress;

    @SerializedName("Type")
        private String Type;

    @SerializedName("CustomerSegment")
        private String[] CustomerSegment;

    @SerializedName("Accessibility")
        private String[] Accessibility;

    @SerializedName("SequenceNumber")
        private String SequenceNumber;

    @SerializedName("Identification")
        private String Identification;
    @SerializedName("ContactInfo")
        private ContactInfo[] ContactInfo;
    @SerializedName("Name")
        private String Name;

        private OtherServiceAndFacility[] OtherServiceAndFacility;

        public Availability getAvailability ()
        {
            return Availability;
        }

        public void setAvailability (Availability Availability)
        {
            this.Availability = Availability;
        }

        public PostalAddress getPostalAddress ()
        {
            return PostalAddress;
        }

        public void setPostalAddress (PostalAddress PostalAddress)
        {
            this.PostalAddress = PostalAddress;
        }

        public String getType ()
        {
            return Type;
        }

        public void setType (String Type)
        {
            this.Type = Type;
        }

        public String[] getCustomerSegment ()
        {
            return CustomerSegment;
        }

        public void setCustomerSegment (String[] CustomerSegment)
        {
            this.CustomerSegment = CustomerSegment;
        }

        public String[] getAccessibility ()
        {
            return Accessibility;
        }

        public void setAccessibility (String[] Accessibility)
        {
            this.Accessibility = Accessibility;
        }

        public String getSequenceNumber ()
        {
            return SequenceNumber;
        }

        public void setSequenceNumber (String SequenceNumber)
        {
            this.SequenceNumber = SequenceNumber;
        }

        public String getIdentification ()
        {
            return Identification;
        }

        public void setIdentification (String Identification)
        {
            this.Identification = Identification;
        }

        public ContactInfo[] getContactInfo ()
        {
            return ContactInfo;
        }

        public void setContactInfo (ContactInfo[] ContactInfo)
        {
            this.ContactInfo = ContactInfo;
        }

        public String getName ()
        {
            return Name;
        }

        public void setName (String Name)
        {
            this.Name = Name;
        }

        public OtherServiceAndFacility[] getOtherServiceAndFacility ()
        {
            return OtherServiceAndFacility;
        }

        public void setOtherServiceAndFacility (OtherServiceAndFacility[] OtherServiceAndFacility)
        {
            this.OtherServiceAndFacility = OtherServiceAndFacility;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Availability = "+Availability+", PostalAddress = "+PostalAddress+", Type = "+Type+", CustomerSegment = "+CustomerSegment+", Accessibility = "+Accessibility+", SequenceNumber = "+SequenceNumber+", Identification = "+Identification+", ContactInfo = "+ContactInfo+", Name = "+Name+", OtherServiceAndFacility = "+OtherServiceAndFacility+"]";
        }
    }


