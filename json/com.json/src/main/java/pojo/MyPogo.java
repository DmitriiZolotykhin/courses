package pojo;

import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

public class MyPogo implements Serializable {

    @SerializedName("data")
        private Data[] data;

        public Data[] getData ()
        {
            return data;
        }

        public void setData (Data[] data)
        {
            this.data = data;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [data = "+data+"]";
        }


}


