package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StandardAvailability implements Serializable {
    @SerializedName("Day")
        private Day[] Day;

        public Day[] getDay ()
        {
            return Day;
        }

        public void setDay (Day[] Day)
        {
            this.Day = Day;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Day = "+Day+"]";
        }
    }


