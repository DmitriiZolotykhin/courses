package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtherServiceAndFacility implements Serializable {
    @SerializedName("Code")
        private String Code;
    @SerializedName("Name")
        private String Name;

        public String getCode ()
        {
            return Code;
        }

        public void setCode (String Code)
        {
            this.Code = Code;
        }

        public String getName ()
        {
            return Name;
        }

        public void setName (String Name)
        {
            this.Name = Name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Code = "+Code+", Name = "+Name+"]";
        }
    }


