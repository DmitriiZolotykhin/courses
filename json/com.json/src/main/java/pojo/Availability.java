package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Availability implements Serializable {


    @SerializedName("StandardAvailability")
        private StandardAvailability StandardAvailability;

        public StandardAvailability getStandardAvailability ()
        {
            return StandardAvailability;
        }

        public void setStandardAvailability (StandardAvailability StandardAvailability)
        {
            this.StandardAvailability = StandardAvailability;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [StandardAvailability = "+StandardAvailability+"]";
        }
    }


