package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Day implements Serializable {
    @SerializedName("name")
        private String Name;
    @SerializedName("OpeningHours")
        private OpeningHours[] OpeningHours;

        public String getName ()
        {
            return Name;
        }

        public void setName (String Name)
        {
            this.Name = Name;
        }

        public OpeningHours[] getOpeningHours ()
        {
            return OpeningHours;
        }

        public void setOpeningHours (OpeningHours[] OpeningHours)
        {
            this.OpeningHours = OpeningHours;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Name = "+Name+", OpeningHours = "+OpeningHours+"]";
        }
    }


