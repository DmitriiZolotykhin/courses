package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OpeningHours implements Serializable {
    @SerializedName("ClosingTime")
        private String ClosingTime;
    @SerializedName("OpeningTime")
        private String OpeningTime;

        public String getClosingTime ()
        {
            return ClosingTime;
        }

        public void setClosingTime (String ClosingTime)
        {
            this.ClosingTime = ClosingTime;
        }

        public String getOpeningTime ()
        {
            return OpeningTime;
        }

        public void setOpeningTime (String OpeningTime)
        {
            this.OpeningTime = OpeningTime;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [ClosingTime = "+ClosingTime+", OpeningTime = "+OpeningTime+"]";
        }
    }


