package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GeographicCoordinates implements Serializable {

    @SerializedName("Latitude")
        private String Latitude;
    @SerializedName("Longitude")
        private String Longitude;

        public String getLatitude ()
        {
            return Latitude;
        }

        public void setLatitude (String Latitude)
        {
            this.Latitude = Latitude;
        }

        public String getLongitude ()
        {
            return Longitude;
        }

        public void setLongitude (String Longitude)
        {
            this.Longitude = Longitude;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Latitude = "+Latitude+", Longitude = "+Longitude+"]";
        }
    }



