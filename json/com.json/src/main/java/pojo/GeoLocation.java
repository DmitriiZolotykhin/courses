package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GeoLocation implements Serializable {
    @SerializedName("GeographicCoordinates")
        private GeographicCoordinates GeographicCoordinates;

        public GeographicCoordinates getGeographicCoordinates ()
        {
            return GeographicCoordinates;
        }

        public void setGeographicCoordinates (GeographicCoordinates GeographicCoordinates)
        {
            this.GeographicCoordinates = GeographicCoordinates;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [GeographicCoordinates = "+GeographicCoordinates+"]";
        }
    }


