package pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContactInfo implements Serializable {
    @SerializedName("ContactType")
        private String ContactType;

    @SerializedName("ContactContent")
        private String ContactContent;

        public String getContactType ()
        {
            return ContactType;
        }

        public void setContactType (String ContactType)
        {
            this.ContactType = ContactType;
        }

        public String getContactContent ()
        {
            return ContactContent;
        }

        public void setContactContent (String ContactContent)
        {
            this.ContactContent = ContactContent;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [ContactType = "+ContactType+", ContactContent = "+ContactContent+"]";
        }
    }

