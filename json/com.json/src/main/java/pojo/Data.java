package pojo;

import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

public class Data implements Serializable {

    @SerializedName("Brand")

        private Brand[] Brand;

        public Brand[] getBrand ()
        {
            return Brand;
        }

        public void setBrand (Brand[] Brand)
        {
            this.Brand = Brand;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Brand = "+Brand+"]";
        }
    }


