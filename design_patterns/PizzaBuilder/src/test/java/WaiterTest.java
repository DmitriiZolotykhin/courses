import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WaiterTest {

    @Test
    public void given_Waiter_when_HawaiianBuilderSet_then_ReturnsHawaiianPizza() {
        Waiter waiter = new Waiter();

        waiter.setBuilder(new HawaiianShop());
        Pizza pizza = waiter.getPizza();

        assertEquals(Constans.HAWAIIAN_TOPPING, pizza.getTopping());
        assertEquals(Constans.HAWAIIAN_SAUCE, pizza.getSauce());
        assertEquals(Constans.HAWAIIAN_DOUGH, pizza.getDough());
    }
}
