package Main;

import builder_bar.CoffeBeverageBuilder;
import builder_maker.AmericanMaker;
import builder_shop.AmericanShop;
import builder_shop.HawaiianShop;
import builder_shop.MargheritaShop;
import waiter.Waiter;

import java.util.Scanner;

public class Main {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        Waiter waiter = new Waiter();
        System.err.println("Choose your pizza: \n1.American\n2.Hawaiian\n3.Margherita\n4.American_from_Chef");
        System.err.println("Choose your beverage: \n5.Coffe\n6.Tea(NOT)\n7.Beer(NOT)");
        int choice = scanner.nextInt();
        boolean condition = true;
        while (condition) {
            if (choice > 0 && choice < 6) {
                switch (choice) {
                    case 1:
                        waiter.setBuilder(new AmericanShop());
                        System.out.println("Your choice is: " + "\n" + waiter.getPizza()+" Amerikan pizza from Shop");
                        condition = false;
                        break;
                    case 2:
                        waiter.setBuilder(new HawaiianShop());
                        System.out.println("Your choice is: " + "\n" + waiter.getPizza()+ " Hawaiian pizza from Shop");
                        condition = false;
                        break;
                    case 3:
                        waiter.setBuilder(new MargheritaShop());
                        System.out.println("Your choice is: " + "\n" + waiter.getPizza()+ " Margherita pizza from Shop");
                        condition = false;
                        break;

                    case 4:
                        waiter.setBuilder(new AmericanMaker());
                        System.out.println("Your choice is: " + "\n" + waiter.getPizza() + " American pizza from Maker");
                        condition = false;
                        break;

                    case 5:
                        waiter.setBuilder2(new CoffeBeverageBuilder());
                        System.out.println("Your choice is: " + "\n" + waiter.getBeverage() + " Coffe from Bar");
                        condition = false;
                        break;
                    default:
                        System.err.println("Wrong choice, type again");

                }
            } else {
                System.err.println("Wrong number, try again");
                break;
            }
        }
    }
}
