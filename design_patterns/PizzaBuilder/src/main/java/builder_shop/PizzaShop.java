package builder_shop;

import product.Pizza;
import waiter.PizzaService;

public abstract class PizzaShop implements PizzaService {

    protected Pizza pizza;
    private PizzaShop builder;

    public PizzaShop() {
        pizza = new Pizza();
    }

    public Pizza getPizza() {

        return pizza;
    }

}