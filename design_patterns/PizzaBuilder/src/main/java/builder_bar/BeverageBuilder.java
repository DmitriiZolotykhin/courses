package builder_bar;


import product.Beverage;

public abstract class BeverageBuilder {

    protected Beverage beverage;

    public BeverageBuilder() {
        beverage = new Beverage();
    }

    public abstract Beverage buildCoffe();


    public Beverage getBeverage() {
        return beverage;



    }
}
