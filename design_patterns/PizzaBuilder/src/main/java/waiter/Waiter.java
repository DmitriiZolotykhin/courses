package waiter;

        import builder_bar.BeverageBuilder;
        import builder_maker.AmericanMaker;
        import product.Beverage;
        import product.Pizza;

public class Waiter {
    private PizzaService pizzaService;

    public void setBuilder(PizzaService builder) {
        this.pizzaService = pizzaService;
    }

    public Pizza getPizza() {
        return PizzaService.getPizza();
    }

    private BeverageBuilder builder2;

    public void setBuilder2(BeverageBuilder builder2) {
        this.builder2 = builder2;
    }

    public Beverage getBeverage() {
        builder2.buildCoffe();
        return builder2.getBeverage();
    }


}
