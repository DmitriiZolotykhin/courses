public class Terminal {

    private PayBuilder builder;


    public void setBuilder(PayBuilder builder) {
        this.builder = builder;
    }

    public Pay getPay() {
        builder.buildDiscount();
        return builder.getPay();
    }

    private CardBuilder builder2;

    public void setBuilder2(CardBuilder builder2) {
        this.builder2 = builder2;
    }

    public Card getCard() {
        builder2.buildDiscount();
        return builder2.getCard();
    }


}
