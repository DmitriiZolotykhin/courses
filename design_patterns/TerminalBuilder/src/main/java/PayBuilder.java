public abstract class PayBuilder {


    protected Pay pay;

    public PayBuilder() {
        pay = new Pay();
    }

    public abstract void buildDiscount();

    public Pay getPay() {
        return pay;
    }
}
