public class DiscountForPayCash extends PayBuilder {
    public void buildDiscount() {
        pay.setDiscount("10% discount on next sale, when paying by cash");
    }

}
