public class DiscountForPayCard extends CardBuilder {
    public void buildDiscount() {
        card.setDiscount("10% discount on next sale, when paying by card");
    }

}
