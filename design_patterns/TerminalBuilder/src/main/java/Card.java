public class Card {


    private String discount;

    public String getDiscount() {
        return discount;
    }


    public void setDiscount(String discount) {
        this.discount = discount;
    }


    @Override
    public String toString() {
        return "Card: " +
                "\nCard - " + discount;
    }
}
