/**
 * Created by Администратор on 15.09.2019.
 */
public abstract class CardBuilder {

    protected Card card;

    public CardBuilder() {
        card = new Card();
    }


    public Card getCard() {
        return card;
    }

    public abstract void buildDiscount();
}
