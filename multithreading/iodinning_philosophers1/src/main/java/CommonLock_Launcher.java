import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static utils.Fair.*;

public class CommonLock_Launcher {
    private static final Logger logger = Logger.getGlobal();

    public static int N = 5;

    public static void main(String[] args) throws InterruptedException {

        run(0, 0);
        run(0, 1);
        run(1, 0);
        run(1, 1);
        run(10, 10);
        run(1, 100);
        run(100, 1);
        run(1000000, 100);
        run(1000, 1000000);
    }

    private static void run(long eatTime, long thinkTime) {
        System.out.println("Test: eat= " + eatTime + "ms, think = " + thinkTime + "ms");

        new CommonLockEngine(N, FAIR, eatTime, thinkTime).newRun();


    }

}


