

import java.util.concurrent.locks.Lock;

import utils.Util;

import java.util.concurrent.atomic.AtomicInteger;


public class Philosopher implements Runnable {

    private final Lock waiter;
    private final AtomicInteger eaten;
    private final long eatTime;
    private final long thinkTime;
    private int count;

    public Philosopher(Lock waiter, AtomicInteger eaten, long eatTime, long thinkTime) {
        this.waiter = waiter;
        this.eaten = eaten;
        this.eatTime = eatTime;
        this.thinkTime = thinkTime;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                waiter.lock();
                eat();
            } finally {
                waiter.unlock();
                think();
            }
        }
        eaten.set(count);

    }

    private void eat() {
        count++;
        Util.waitMillis(eatTime);
    }

    private void think() {
        Util.waitMillis(thinkTime);
    }

}
