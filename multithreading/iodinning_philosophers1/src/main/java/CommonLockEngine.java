

import utils.Fair;
import utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class CommonLockEngine {


    static final int SECONDS = 2;

    final List<Philosopher> philosophers = new ArrayList<>();
    private final int N;
    private final Fair fair;
    private final long eatTime;
    private final long thinkTime;


    public CommonLockEngine(int n, Fair fair, long eatTime, long thinkTime) {
        this.N = n;
        this.fair = fair;
        this.eatTime = eatTime;
        this.thinkTime = thinkTime;
    }

    public void newRun() {
        ReentrantLock commonLock = new ReentrantLock(Fair.FAIR.equals(fair));
        AtomicInteger[] eaten = new AtomicInteger[N];
        for (int i = 0; i < N; i++) {
            eaten[i] = new AtomicInteger(0);
            philosophers.add(new Philosopher(commonLock, eaten[i], eatTime, thinkTime));
        }
        List<Thread> philosopherThreads = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            Thread philosopherThread = new Thread(philosophers.get(i));
            philosopherThreads.add(philosopherThread);
            philosopherThread.start();
            System.out.println(toString(eaten) + "" + i + "" + philosophers + "" + thinkTime);
            //System.out.println(thinkTime);

        }

        Util.waitMillis(1000L * SECONDS);
        philosopherThreads.stream().forEach(Thread::interrupt);

        Util.waitMillis(100);
        Util.printResult(eaten);
    }

    private String toString(List<Philosopher> philosophers) {
        return new String();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommonLockEngine that = (CommonLockEngine) o;

        if (N != that.N) return false;
        if (eatTime != that.eatTime) return false;
        if (thinkTime != that.thinkTime) return false;
        if (!philosophers.equals(that.philosophers)) return false;
        return fair == that.fair;
    }

    @Override
    public int hashCode() {
        int result = philosophers.hashCode();
        result = 31 * result + N;
        result = 31 * result + (fair != null ? fair.hashCode() : 0);
        result = 31 * result + (int) (eatTime ^ (eatTime >>> 32));
        result = 31 * result + (int) (thinkTime ^ (thinkTime >>> 32));
        return result;
    }

    private String toString(AtomicInteger[] eaten) {
        return new String();
    }

}
