/**
 * Created by Администратор on 11.09.2019.
 */
public class BankUser {
    private String name;

    public BankUser(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }}
