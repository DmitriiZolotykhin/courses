import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Администратор on 11.09.2019.
 */
public class ThreadExercise extends Thread {

    private BankUser bankUser;
    Random rnd = new Random();
    int amount = rnd.nextInt();


    public ThreadExercise(BankUser p) {
        this.bankUser = p;
    }

    public static void main(String[] args) {

        ThreadExercise ts1 = new ThreadExercise(new BankUser("bankUser 1"));
        ts1.start();
        ThreadExercise ts2 = new ThreadExercise(new BankUser("bankUser 2"));
        ts2.start();
        ThreadExercise ts3 = new ThreadExercise(new BankUser("bankUser 3"));
        ts3.start();

    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            try {
                Bank acc = Bank.getAccount(bankUser);
                acc.withdraw(990);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadExercise.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (acc.getBal() < 0) {
                    System.exit(0);
                }
                // acc.deposit(200);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Окончательный баланс " + Bank.getBal());
        System.exit(0);
        getThreadGroup().stop();

    }
}

