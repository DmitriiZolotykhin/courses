/**
 * Created by Администратор on 11.09.2019.
 */
public class Bank {
    public static Bank account;
    private static volatile int moneyAmount = 1000;
    private static BankUser person;

    private Bank() {
    }

    public static Bank getAccount(BankUser p) {
        if (account == null) {
            account = new Bank();
        }
        Bank.person = p;
        return account;
    }

    public static int getBal() {
        return moneyAmount;
    }

    public void transferMoney(int bal) {
        int withdrawResult = moneyAmount - bal;
        if (bal > 0) {

            if (withdrawResult >= 0) {
                moneyAmount = withdrawResult;
            } else {


                throw new NotEnoughMoneyException("Нет больше денег в банке" +
                        " хотел снять " + bal + " сумма в банке " + moneyAmount);
            }
        } else {
            throw new NotEnoughMoneyException("Отрицательное значение");

        }
    }

    public boolean hasMoney(int getBal) {
        return moneyAmount >= getBal();
    }

    public synchronized void withdraw(int bal) {
        try {

            if (hasMoney(bal)) {
                System.out.println(person.getName() + " " + " хочет снять " + bal + " из банка, а в банке " + ""+moneyAmount);


                try {
                    transferMoney(bal);
                } catch (NotEnoughMoneyException e) {
                    System.exit(0);
                }
                System.out.println(person.getName() + " " + "снятие завершено в сумме" + " " + bal);
            } else {
                System.out.println("Нет денег в банке");
                System.exit(0);
            }


            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(person.getName() + " " + " в банке" + " " + moneyAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

