/**
 * Created by Администратор on 11.09.2019.
 */
public class NotEnoughMoneyException extends RuntimeException {

    public NotEnoughMoneyException(String message) {
        super(message);
    }
}

